<main>
    <!-- tidak dipakai -->
    <!-- <div class="container-fluid py-4">
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    <div class="col-xl-6 mb-xl-0 mb-4">
                        <div class="card bg-transparent shadow-xl">
                            <div class="overflow-hidden position-relative border-radius-xl"
                                style="background-image: url('../assets/img/curved-images/curved14.jpg');">
                                <span class="mask bg-gradient-dark"></span>
                                <div class="card-body position-relative z-index-1 p-3">
                                    <i class="fas fa-wifi text-white p-2"></i>
                                    <h5 class="text-white mt-4 mb-5 pb-2">
                                        4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                    <div class="d-flex">
                                        <div class="d-flex">
                                            <div class="me-4">
                                                <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                <h6 class="text-white mb-0">Jack Peterson</h6>
                                            </div>
                                            <div>
                                                <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                <h6 class="text-white mb-0">11/22</h6>
                                            </div>
                                        </div>
                                        <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                            <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header mx-4 p-3 text-center">
                                        <div
                                            class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                                            <i class="fas fa-landmark opacity-10"></i>
                                        </div>
                                    </div>
                                    <div class="card-body pt-0 p-3 text-center">
                                        <h6 class="text-center mb-0">Salary</h6>
                                        <span class="text-xs">Belong Interactive</span>
                                        <hr class="horizontal dark my-3">
                                        <h5 class="mb-0">+$2000</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card">
                                    <div class="card-header mx-4 p-3 text-center">
                                        <div
                                            class="icon icon-shape icon-lg bg-gradient-primary shadow text-center border-radius-lg">
                                            <i class="fab fa-paypal opacity-10"></i>
                                        </div>
                                    </div>
                                    <div class="card-body pt-0 p-3 text-center">
                                        <h6 class="text-center mb-0">Paypal</h6>
                                        <span class="text-xs">Freelance Payment</span>
                                        <hr class="horizontal dark my-3">
                                        <h5 class="mb-0">$455.00</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mb-lg-0 mb-4">
                        <div class="card mt-4">
                            <div class="card-header pb-0 p-3">
                                <div class="row">
                                    <div class="col-md-6 d-flex align-items-center">
                                        <h6 class="mb-0">Payment Method</h6>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <a class="btn bg-gradient-dark mb-0" href="javascript:;"><i
                                                class="fas fa-plus"></i>&nbsp;&nbsp;Add New Card</a>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-3">
                                <div class="row">
                                    <div class="col-md-6 mb-md-0 mb-4">
                                        <div
                                            class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                                            <img class="w-10 me-3 mb-0" src="../assets/img/logos/mastercard.png"
                                                alt="logo">
                                            <h6 class="mb-0">
                                                ****&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;&nbsp;7852
                                            </h6>
                                            <i class="fas fa-pencil-alt ms-auto text-dark cursor-pointer"
                                                data-bs-toggle="tooltip" data-bs-placement="top" title="Edit Card"></i>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div
                                            class="card card-body border card-plain border-radius-lg d-flex align-items-center flex-row">
                                            <img class="w-10 me-3 mb-0" src="../assets/img/logos/visa.png" alt="logo">
                                            <h6 class="mb-0">
                                                ****&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;&nbsp;****&nbsp;&nbsp;&nbsp;5248
                                            </h6>
                                            <i class="fas fa-pencil-alt ms-auto text-dark cursor-pointer"
                                                data-bs-toggle="tooltip" data-bs-placement="top" title="Edit Card"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="card h-100">
                    <div class="card-header pb-0 p-3">
                        <div class="row">
                            <div class="col-md-6 d-flex align-items-center">
                                <h6 class="mb-0">Invoices</h6>
                            </div>
                            <div class="col-md-6 text-right">
                                <button class="btn btn-outline-primary btn-sm mb-0">View All</button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body p-3 pb-0">
                        <ul class="list-group">
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="mb-1 text-dark font-weight-bold text-sm">March, 01, 2020</h6>
                                    <span class="text-xs">#CC-214589</span>
                                </div>
                                <div class="d-flex align-items-center text-sm">
                                    $350
                                    <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i
                                            class="fas fa-file-pdf text-lg me-1"></i> PDF</button>
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="text-dark mb-1 font-weight-bold text-sm">April, 05, 2020</h6>
                                    <span class="text-xs">#FB-212562</span>
                                </div>
                                <div class="d-flex align-items-center text-sm">
                                    $560
                                    <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i
                                            class="fas fa-file-pdf text-lg me-1"></i> PDF</button>
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="text-dark mb-1 font-weight-bold text-sm">June, 25, 2019</h6>
                                    <span class="text-xs">#QW-103578</span>
                                </div>
                                <div class="d-flex align-items-center text-sm">
                                    $120
                                    <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i
                                            class="fas fa-file-pdf text-lg me-1"></i> PDF</button>
                                </div>
                            </li>
                            <li class="list-group-item border-0 d-flex justify-content-between ps-0 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="text-dark mb-1 font-weight-bold text-sm">March, 01, 2019</h6>
                                    <span class="text-xs">#AR-803481</span>
                                </div>
                                <div class="d-flex align-items-center text-sm">
                                    $300
                                    <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i
                                            class="fas fa-file-pdf text-lg me-1"></i> PDF</button>
                                </div>
                            </li>
                            <li class="list-group-item border-0 d-flex justify-content-between ps-0 border-radius-lg">
                              <div class="d-flex flex-column">
                                  <h6 class="text-dark mb-1 font-weight-bold text-sm">March, 01, 2019</h6>
                                  <span class="text-xs">#ST-451897</span>
                              </div>
                              <div class="d-flex align-items-center text-sm">
                                  $275
                                  <button class="btn btn-link text-dark text-sm mb-0 px-0 ms-4"><i
                                          class="fas fa-file-pdf text-lg me-1"></i> PDF</button>
                              </div>
                          </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 mt-4">
                <div class="card">
                    <div class="card-header pb-0 px-3">
                        <h6 class="mb-0">Billing Information</h6>
                    </div>
                    <div class="card-body pt-4 p-3">
                        <ul class="list-group">
                            <li class="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="mb-3 text-sm">Oliver Liam</h6>
                                    <span class="mb-2 text-xs">Company Name: <span
                                            class="text-dark font-weight-bold ms-2">Viking Burrito</span></span>
                                    <span class="mb-2 text-xs">Email Address: <span
                                            class="text-dark ms-2 font-weight-bold">oliver@burrito.com</span></span>
                                    <span class="text-xs">VAT Number: <span
                                            class="text-dark ms-2 font-weight-bold">FRB1235476</span></span>
                                </div>
                                <div class="ms-auto">
                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i
                                            class="far fa-trash-alt me-2"></i>Delete</a>
                                    <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i
                                            class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                </div>
                            </li>
                            <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="mb-3 text-sm">Lucas Harper</h6>
                                    <span class="mb-2 text-xs">Company Name: <span
                                            class="text-dark font-weight-bold ms-2">Stone Tech Zone</span></span>
                                    <span class="mb-2 text-xs">Email Address: <span
                                            class="text-dark ms-2 font-weight-bold">lucas@stone-tech.com</span></span>
                                    <span class="text-xs">VAT Number: <span
                                            class="text-dark ms-2 font-weight-bold">FRB1235476</span></span>
                                </div>
                                <div class="ms-auto">
                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i
                                            class="far fa-trash-alt me-2"></i>Delete</a>
                                    <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i
                                            class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                </div>
                            </li>
                            <li class="list-group-item border-0 d-flex p-4 mb-2 mt-3 bg-gray-100 border-radius-lg">
                                <div class="d-flex flex-column">
                                    <h6 class="mb-3 text-sm">Ethan James</h6>
                                    <span class="mb-2 text-xs">Company Name: <span
                                            class="text-dark font-weight-bold ms-2">Fiber Notion</span></span>
                                    <span class="mb-2 text-xs">Email Address: <span
                                            class="text-dark ms-2 font-weight-bold">ethan@fiber.com</span></span>
                                    <span class="text-xs">VAT Number: <span
                                            class="text-dark ms-2 font-weight-bold">FRB1235476</span></span>
                                </div>
                                <div class="ms-auto">
                                    <a class="btn btn-link text-danger text-gradient px-3 mb-0" href="javascript:;"><i
                                            class="far fa-trash-alt me-2"></i>Delete</a>
                                    <a class="btn btn-link text-dark px-3 mb-0" href="javascript:;"><i
                                            class="fas fa-pencil-alt text-dark me-2" aria-hidden="true"></i>Edit</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-5 mt-4">
                <div class="card h-100 mb-4">
                    <div class="card-header pb-0 px-3">
                        <div class="row">
                            <div class="col-md-6">
                                <h6 class="mb-0">Your Transaction's</h6>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end align-items-center">
                                <i class="far fa-calendar-alt me-2"></i>
                                <small>23 - 30 March 2020</small>
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-4 p-3">
                        <h6 class="text-uppercase text-body text-xs font-weight-bolder mb-3">Newest</h6>
                        <ul class="list-group">
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-danger mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-down"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Netflix</h6>
                                        <span class="text-xs">27 March 2020, at 12:30 PM</span>
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center text-danger text-gradient text-sm font-weight-bold">
                                    - $ 2,500
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Apple</h6>
                                        <span class="text-xs">27 March 2020, at 04:30 AM</span>
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                    + $ 2,000
                                </div>
                            </li>
                        </ul>
                        <h6 class="text-uppercase text-body text-xs font-weight-bolder my-3">Yesterday</h6>
                        <ul class="list-group">
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Stripe</h6>
                                        <span class="text-xs">26 March 2020, at 13:45 PM</span>
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                    + $ 750
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">HubSpot</h6>
                                        <span class="text-xs">26 March 2020, at 12:30 PM</span>
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                    + $ 1,000
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-success mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-arrow-up"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Creative Tim</h6>
                                        <span class="text-xs">26 March 2020, at 08:30 AM</span>
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center text-success text-gradient text-sm font-weight-bold">
                                    + $ 2,500
                                </div>
                            </li>
                            <li
                                class="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                                <div class="d-flex align-items-center">
                                    <button
                                        class="btn btn-icon-only btn-rounded btn-outline-dark mb-0 me-3 btn-sm d-flex align-items-center justify-content-center"><i
                                            class="fas fa-exclamation"></i></button>
                                    <div class="d-flex flex-column">
                                        <h6 class="mb-1 text-dark text-sm">Webflow</h6>
                                        <span class="text-xs">26 March 2020, at 05:00 AM</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center text-dark text-sm font-weight-bold">
                                    Pending
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- kontrol -->
    <div class="container-fluid py-4">
    <div class="row mb-4">
        <!-- <div class="col-12">
            <div class="card h-100">
                <div class="card-header pb-0 p-3">
                    <h6 class="mb-0">Platform Settings</h6>
                </div>
                <div class="card-body p-3">
                    <h6 class="text-uppercase text-body text-xs font-weight-bolder">Account</h6>
                    <ul class="list-group">
                        <li class="list-group-item border-0 px-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault"
                                    checked>
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault">Email me when someone follows me</label>
                            </div>
                        </li>
                        <li class="list-group-item border-0 px-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault1">
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault1">Email me when someone answers on my post</label>
                            </div>
                        </li>
                        <li class="list-group-item border-0 px-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault2"
                                    checked>
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault2">Email me when someone mentions me</label>
                            </div>
                        </li>
                    </ul>
                    <h6 class="text-uppercase text-body text-xs font-weight-bolder mt-4">Application</h6>
                    <ul class="list-group">
                        <li class="list-group-item border-0 px-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault3">
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault3">New launches and projects</label>
                            </div>
                        </li>
                        <li class="list-group-item border-0 px-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault4"
                                    checked>
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault4">Monthly product updates</label>
                            </div>
                        </li>
                        <li class="list-group-item border-0 px-0 pb-0">
                            <div class="form-check form-switch ps-0">
                                <input class="form-check-input ms-auto" type="checkbox" id="flexSwitchCheckDefault5">
                                <label class="form-check-label text-body ms-3 text-truncate w-80 mb-0"
                                    for="flexSwitchCheckDefault5">Subscribe to newsletter</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <!-- harga material tambang -->
        <div class="col-12">
            <div class="card">
            <div class="card-header pb-0 p-3">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0">Harga Material Tambang</h5>
                        </div>
                        <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; Tambah Data</a>
                    </div>
                </div>
                <div class="card-body">
                    <!-- <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Belum di Cek</button>
                            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Sudah di Cek</button>
                            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</button>
                        </div>
                    </nav> -->
                    <div class="tab-content" id="nav-tabContent">
                        <!-- belum dicek -->
                        <div class="row tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="col-12">
                                    <div class="card">
                                            <!-- <div class="card-header"> -->
                                                <!-- <div class="row col-sm-12">
                                                    date -->
                                                    <!-- <div class="col-md-2 offset-5 px-0 pr-0"> -->
                                                    <!-- <div class="input-group">
                                                                <span class="input-group-text text-body"><i class="fas fa-calendar text-center" aria-hidden="true"></i></span>
                                                                <input class="form-control bg-white" type="date" id="tanggal" name="tanggal" data-toggle="tooltip" title="Pilih Tanggal">
                                                            </div>
                                                    </div> -->
                                                    <!-- button dropdown Waktu -->
                                                    <!-- <div class="col-md-2 px-0-left pr-0-right">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Waktu</span>
                                                                <input type="time" class="form-control bg-white" placeholder="plat" data-toggle="tooltop" title="Masukkan Waktu"></input>
                                                            </div>
                                                    </div> -->
                                                    <!-- button dropdown Plat Kendaraan -->
                                                    <!-- <div class="col-md-2 px-0 pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Plat</span>
                                                                <input type="text" class="form-control bg-white" placeholder="Tulis Disini...">
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <!-- button dropdown ijin tambang -->
                                                    <!-- <div class="col-md-3 px-1-left pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Ijin Tambang</span>
                                                                <select type="text" class="form-control bg-white" placeholder="Pilih...">
                                                                <option selected>Pilih..</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                    <!-- button cari -->
                                                    <!-- <div class="col-md-1 px-0-left pr-0 d-flex justify-content-end">
                                                        <a href="" class="input-group-text text-body bg-danger"><i class="fas fa-search text-center text-white" aria-hidden="true"></i></a>
                                                    </div> -->
                                                    <!-- <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a> -->
                                                <!-- </div> -->
                                            <!-- </div> -->
                                            <!-- list isi belum di cek -->
                                            <div class="card-body px-0 pt-4 pb-2">
                                                <div class="table-responsive p-0">
                                                    <table class="table align-items-center mb-0">
                                                        <!-- nama kolom -->
                                                        <thead>
                                                            <tr>
                                                                <th class="text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Nomor
                                                                </th>
                                                                <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                                    Photo
                                                                </th> -->
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Nama Material
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Harga Lama
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Harga Baru
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Tanggal Perubahan
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <!-- isi kolom -->
                                                        <tbody>
                                                            <!-- 1 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">1</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">08.50.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 1234 CD</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal1" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade fade bd-example-modal-xl" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-xl">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-6 text-left">
                                                                                            <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                        </div>
                                                                                        <div class="col-md-6 text-right">
                                                                                            <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="container-fluid">
                                                                                        <div class="card">
                                                                                            <div class="row">
                                                                                                <!-- informasi transaksi -->
                                                                                                <div class="col-md-5">
                                                                                                    <form class="mt-4 ml-4" role="form text-left">
                                                                                                        <!-- Tanggal dan Waktu -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 08.50.00 WIB</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- nama pt -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Plat Kendaraan -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">AB 1234 CD</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Harga/Ton -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Jenisgalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- tonasegalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- pajak -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- total Harga -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Status -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Status </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group text-left">
                                                                                                                    <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- rencana tambang -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Rencana Tambang </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                <div class="progress-wrapper w-95">
                                                                                                                    <div class="progress-gradient-success">
                                                                                                                        <div class="progress-percentage">
                                                                                                                        <span class="text-xs font-weight-bold">20% <i class="fas fa-check-circle text-success"></i></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="progress">
                                                                                                                        <div class="progress-bar bg-gradient-success w-20" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                                <!-- sebelahnya -->
                                                                                                    <div class="row col-md-7">
                                                                                                        <div class="col-lg-7 mb-0 p-2">
                                                                                                            <div class="card bg-transparent shadow-xl">
                                                                                                                <div class="overflow-hidden position-relative border-radius-xl"
                                                                                                                    style="background-image: url('../assets/img/curved-images/batik4.jpg');">
                                                                                                                    <span class="mask bg-gradient-dark"></span>
                                                                                                                    <div class="card-body position-relative z-index-1 p-3">
                                                                                                                        <i class="fas fa-wifi text-white p-2 pb-2 float-start"></i>
                                                                                                                        <h5 class="text-white mt-5 mb-5 pb-2 text-left">
                                                                                                                            4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                                                                                                        <div class="d-flex">
                                                                                                                                <div class="d-flex">
                                                                                                                                    <div class="me-4">
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                                                                                                        <h6 class="text-white mb-0">PT. RJ Abadi</h6>
                                                                                                                                    </div>
                                                                                                                                    <div>
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                                                                                                        <h6 class="text-white mb-0">11/22</h6>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                                                                                                                    <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- lokasi -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Lokasi </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Tambang I</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- status -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Status </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Operasional <i class="fas fa-check-circle text-success"></i></p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- material -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Material </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Batu Andesit</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- expired -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Expired </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> 17 Mei 2026 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- Saldo -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Saldo </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Rp 50.000.000 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-7 mb-2">
                                                                                                                <div class="page-header min-height-200 border-radius-xl mt-2"
                                                                                                                    style="background-image: url('../assets/img/kir-mobil.jpg'); background-position-y: 100%;">
                                                                                                                </div>
                                                                                                        </div>  
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- berlaku -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Berlaku </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="23 Mei 2024">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- JBB -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBB </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- JBI -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBI </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                    <button type="button" class="btn btn-success">Simpan</button>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <!-- 2 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">2</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">06.45.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 5678 FT</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal2" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade fade bd-example-modal-xl" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-xl">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-6 text-left">
                                                                                            <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                        </div>
                                                                                        <div class="col-md-6 text-right">
                                                                                            <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="container-fluid">
                                                                                        <div class="card">
                                                                                            <div class="row">
                                                                                                <!-- informasi transaksi -->
                                                                                                <div class="col-md-5">
                                                                                                    <form class="mt-4 ml-4" role="form text-left">
                                                                                                        <!-- Tanggal dan Waktu -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 08.50.00 WIB</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- nama pt -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Plat Kendaraan -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">AB 1234 CD</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Harga/Ton -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Jenisgalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- tonasegalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- pajak -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- total Harga -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Status -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Status </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group text-left">
                                                                                                                    <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                                <!-- sebelahnya -->
                                                                                                    <div class="row col-md-7">
                                                                                                        <div class="col-lg-7 mb-0 p-2">
                                                                                                            <div class="card bg-transparent shadow-xl">
                                                                                                                <div class="overflow-hidden position-relative border-radius-xl"
                                                                                                                    style="background-image: url('../assets/img/curved-images/batik4.jpg');">
                                                                                                                    <span class="mask bg-gradient-dark"></span>
                                                                                                                    <div class="card-body position-relative z-index-1 p-3">
                                                                                                                        <i class="fas fa-wifi text-white p-2 pb-2 float-start"></i>
                                                                                                                        <h5 class="text-white mt-5 mb-5 pb-2 text-left">
                                                                                                                            4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                                                                                                        <div class="d-flex">
                                                                                                                                <div class="d-flex">
                                                                                                                                    <div class="me-4">
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                                                                                                        <h6 class="text-white mb-0">PT. RJ Abadi</h6>
                                                                                                                                    </div>
                                                                                                                                    <div>
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                                                                                                        <h6 class="text-white mb-0">11/22</h6>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                                                                                                                    <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- lokasi -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Lokasi </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Tambang I</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- status -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Status </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Operasional <i class="fas fa-check-circle text-success"></i></p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- material -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Material </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Batu Andesit</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- expired -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Expired </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> 17 Mei 2026 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- Saldo -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Saldo </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Rp 50.000.000 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-7 mb-2">
                                                                                                                <div class="page-header min-height-200 border-radius-xl mt-2"
                                                                                                                    style="background-image: url('../assets/img/kir-mobil.jpg'); background-position-y: 100%;">
                                                                                                                </div>
                                                                                                        </div>  
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- berlaku -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Berlaku </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="23 Mei 2024">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- JBB -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBB </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- JBI -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBI </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                    <button type="button" class="btn btn-success">Simpan</button>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <!-- 3 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">3</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Selasa, 02/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">15.50.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 9090 LI</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rp 950.000</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal3" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="row col-12">
                                                                                    <div class="col-md-6 text-left">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                    </div>
                                                                                    <div class="col-md-6 text-right">
                                                                                        <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form role="form text-left">
                                                                                    <!-- Tanggal dan Waktu -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Selasa, 02/11/2021 15.50.00 WIB</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- nama pt -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">PT. Ruby Permata Tunggal</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Plat Kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">AB 9090 LI</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Harga/Ton -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- kir kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Kir Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 px-0">
                                                                                            <div class="form-group">
                                                                                                <input class="form-control form-control-sm" type="text" placeholder="3.000 kg">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4 px-0">
                                                                                            <div class="form-group">
                                                                                            <a href="" class="form-control-label text-center text-sm text-info"> Lihat Gambar Kir. </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Jenisgalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- tonasegalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- pajak -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- total Harga -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Status -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left"> Status </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group text-left">
                                                                                                <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                <button type="button" class="btn btn-success">Simpan</button>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- kendaraan -->
        <div class="col-12 mt-4">
            <div class="card">
            <div class="card-header pb-0 p-3">
                    <div class="d-flex flex-row justify-content-between">
                        <div>
                            <h5 class="mb-0">Kendaraan</h5>
                        </div>
                        <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; Tambah Data</a>
                    </div>
                </div>
                <div class="card-body">
                    <!-- <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home" aria-selected="true">Belum di Cek</button>
                            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" data-bs-target="#nav-profile" type="button" role="tab" aria-controls="nav-profile" aria-selected="false">Sudah di Cek</button>
                            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab" data-bs-target="#nav-contact" type="button" role="tab" aria-controls="nav-contact" aria-selected="false">Contact</button>
                        </div>
                    </nav> -->
                    <div class="tab-content" id="nav-tabContent">
                        <!-- belum dicek -->
                        <div class="row tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="col-12">
                                    <div class="card">
                                            <!-- <div class="card-header">
                                                <div class="row col-sm-12">
                                                    date
                                                    <div class="col-md-2 offset-5 px-0 pr-0">
                                                    <div class="input-group">
                                                                <span class="input-group-text text-body"><i class="fas fa-calendar text-center" aria-hidden="true"></i></span>
                                                                <input class="form-control bg-white" type="date" id="tanggal" name="tanggal" data-toggle="tooltip" title="Pilih Tanggal">
                                                            </div>
                                                    </div>
                                                    button dropdown Waktu
                                                    <div class="col-md-2 px-0-left pr-0-right">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Waktu</span>
                                                                <input type="time" class="form-control bg-white" placeholder="plat" data-toggle="tooltop" title="Masukkan Waktu"></input>
                                                            </div>
                                                    </div>
                                                    button dropdown Plat Kendaraan
                                                    <div class="col-md-2 px-0 pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Plat</span>
                                                                <input type="text" class="form-control bg-white" placeholder="Tulis Disini...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    button dropdown ijin tambang
                                                    <div class="col-md-3 px-1-left pr-0 text-center">
                                                        <div class="d-flex align-items-center">
                                                            <div class="input-group">
                                                                <span class="input-group-text text-body">Ijin Tambang</span>
                                                                <select type="text" class="form-control bg-white" placeholder="Pilih...">
                                                                <option selected>Pilih..</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    button cari
                                                    <div class="col-md-1 px-0-left pr-0 d-flex justify-content-end">
                                                        <a href="" class="input-group-text text-body bg-danger"><i class="fas fa-search text-center text-white" aria-hidden="true"></i></a>
                                                    </div>
                                                    <a href="#" class="btn bg-gradient-primary btn-sm mb-0" type="button">+&nbsp; New User</a>
                                                </div>
                                            </div> -->
                                            <!-- list isi belum di cek -->
                                            <div class="card-body px-0 pt-4 pb-2">
                                                <div class="table-responsive p-0">
                                                    <table class="table align-items-center mb-0">
                                                        <!-- nama kolom -->
                                                        <thead>
                                                            <tr>
                                                                <th class="text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Nomor
                                                                </th>
                                                                <!-- <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">
                                                                    Photo
                                                                </th> -->
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Jenis Kendaraan
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Berat Bersih
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Tanggal Perubahan
                                                                </th>
                                                                <th class="text-center text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 text-capitalize each word">
                                                                    Action
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <!-- isi kolom -->
                                                        <tbody>
                                                            <!-- 1 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">1</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">08.50.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 1234 CD</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal1" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade fade bd-example-modal-xl" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-xl">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-6 text-left">
                                                                                            <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                        </div>
                                                                                        <div class="col-md-6 text-right">
                                                                                            <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="container-fluid">
                                                                                        <div class="card">
                                                                                            <div class="row">
                                                                                                <!-- informasi transaksi -->
                                                                                                <div class="col-md-5">
                                                                                                    <form class="mt-4 ml-4" role="form text-left">
                                                                                                        <!-- Tanggal dan Waktu -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 08.50.00 WIB</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- nama pt -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Plat Kendaraan -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">AB 1234 CD</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Harga/Ton -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Jenisgalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- tonasegalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- pajak -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- total Harga -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Status -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Status </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group text-left">
                                                                                                                    <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- rencana tambang -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Rencana Tambang </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                <div class="progress-wrapper w-95">
                                                                                                                    <div class="progress-gradient-success">
                                                                                                                        <div class="progress-percentage">
                                                                                                                        <span class="text-xs font-weight-bold">20% <i class="fas fa-check-circle text-success"></i></span>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="progress">
                                                                                                                        <div class="progress-bar bg-gradient-success w-20" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                                                                                                                    </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                                <!-- sebelahnya -->
                                                                                                    <div class="row col-md-7">
                                                                                                        <div class="col-lg-7 mb-0 p-2">
                                                                                                            <div class="card bg-transparent shadow-xl">
                                                                                                                <div class="overflow-hidden position-relative border-radius-xl"
                                                                                                                    style="background-image: url('../assets/img/curved-images/batik4.jpg');">
                                                                                                                    <span class="mask bg-gradient-dark"></span>
                                                                                                                    <div class="card-body position-relative z-index-1 p-3">
                                                                                                                        <i class="fas fa-wifi text-white p-2 pb-2 float-start"></i>
                                                                                                                        <h5 class="text-white mt-5 mb-5 pb-2 text-left">
                                                                                                                            4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                                                                                                        <div class="d-flex">
                                                                                                                                <div class="d-flex">
                                                                                                                                    <div class="me-4">
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                                                                                                        <h6 class="text-white mb-0">PT. RJ Abadi</h6>
                                                                                                                                    </div>
                                                                                                                                    <div>
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                                                                                                        <h6 class="text-white mb-0">11/22</h6>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                                                                                                                    <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- lokasi -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Lokasi </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Tambang I</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- status -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Status </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Operasional <i class="fas fa-check-circle text-success"></i></p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- material -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Material </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Batu Andesit</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- expired -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Expired </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> 17 Mei 2026 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- Saldo -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Saldo </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Rp 50.000.000 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-7 mb-2">
                                                                                                                <div class="page-header min-height-200 border-radius-xl mt-2"
                                                                                                                    style="background-image: url('../assets/img/kir-mobil.jpg'); background-position-y: 100%;">
                                                                                                                </div>
                                                                                                        </div>  
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- berlaku -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Berlaku </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="23 Mei 2024">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- JBB -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBB </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- JBI -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBI </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                    <button type="button" class="btn btn-success">Simpan</button>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <!-- 2 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">2</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Rabu, 03/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">06.45.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 5678 FT</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal2" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade fade bd-example-modal-xl" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog modal-xl">
                                                                            <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-6 text-left">
                                                                                            <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                            <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                        </div>
                                                                                        <div class="col-md-6 text-right">
                                                                                            <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-body">
                                                                                    <div class="container-fluid">
                                                                                        <div class="card">
                                                                                            <div class="row">
                                                                                                <!-- informasi transaksi -->
                                                                                                <div class="col-md-5">
                                                                                                    <form class="mt-4 ml-4" role="form text-left">
                                                                                                        <!-- Tanggal dan Waktu -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rabu, 03/11/2021 08.50.00 WIB</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- nama pt -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">PT. RJ Abadi</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Plat Kendaraan -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">AB 1234 CD</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Harga/Ton -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Jenisgalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- tonasegalian -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- pajak -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- total Harga -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <!-- Status -->
                                                                                                        <div class="row col-12">
                                                                                                            <div class="col-md-4">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-left"> Status </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-1 px-0">
                                                                                                                <div class="form-group">
                                                                                                                    <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                            <div class="col-md-7 px-0">
                                                                                                                <div class="form-group text-left">
                                                                                                                    <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </form>
                                                                                                </div>
                                                                                                <!-- sebelahnya -->
                                                                                                    <div class="row col-md-7">
                                                                                                        <div class="col-lg-7 mb-0 p-2">
                                                                                                            <div class="card bg-transparent shadow-xl">
                                                                                                                <div class="overflow-hidden position-relative border-radius-xl"
                                                                                                                    style="background-image: url('../assets/img/curved-images/batik4.jpg');">
                                                                                                                    <span class="mask bg-gradient-dark"></span>
                                                                                                                    <div class="card-body position-relative z-index-1 p-3">
                                                                                                                        <i class="fas fa-wifi text-white p-2 pb-2 float-start"></i>
                                                                                                                        <h5 class="text-white mt-5 mb-5 pb-2 text-left">
                                                                                                                            4562&nbsp;&nbsp;&nbsp;1122&nbsp;&nbsp;&nbsp;4594&nbsp;&nbsp;&nbsp;7852</h5>
                                                                                                                        <div class="d-flex">
                                                                                                                                <div class="d-flex">
                                                                                                                                    <div class="me-4">
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Card Holder</p>
                                                                                                                                        <h6 class="text-white mb-0">PT. RJ Abadi</h6>
                                                                                                                                    </div>
                                                                                                                                    <div>
                                                                                                                                        <p class="text-white text-sm opacity-8 mb-0">Expires</p>
                                                                                                                                        <h6 class="text-white mb-0">11/22</h6>
                                                                                                                                    </div>
                                                                                                                                </div>
                                                                                                                                <div class="ms-auto w-20 d-flex align-items-end justify-content-end">
                                                                                                                                    <img class="w-60 mt-2" src="../assets/img/logos/mastercard.png" alt="logo">
                                                                                                                                </div>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- lokasi -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Lokasi </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Tambang I</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- status -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Status </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark">Operasional <i class="fas fa-check-circle text-success"></i></p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- material -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Material </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Batu Andesit</p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- expired -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Expired </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> 17 Mei 2026 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- Saldo -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Saldo </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm text-dark"> Rp 50.000.000 <i class="fas fa-check-circle text-success"></i> </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                        <div class="col-lg-7 mb-2">
                                                                                                                <div class="page-header min-height-200 border-radius-xl mt-2"
                                                                                                                    style="background-image: url('../assets/img/kir-mobil.jpg'); background-position-y: 100%;">
                                                                                                                </div>
                                                                                                        </div>  
                                                                                                        <div class="col-lg-5">
                                                                                                            <!-- berlaku -->
                                                                                                            <form class="mt-4 ml-4" role="form text-left">
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> Berlaku </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="23 Mei 2024">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </form>
                                                                                                            <!-- JBB -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBB </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            <!-- JBI -->
                                                                                                                <div class="row col-12">
                                                                                                                    <div class="col-md-4">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-left text-sm"> JBI </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-1 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <p class="form-control-label text-center text-sm"> : </p>
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    <div class="col-md-7 px-0">
                                                                                                                        <div class="form-group">
                                                                                                                            <input class="form-control form-control-sm" type="text" placeholder="2.540 Kg">
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                    <button type="button" class="btn btn-success">Simpan</button>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <!-- 3 -->
                                                            <tr>
                                                                <td class="ps-4">
                                                                    <p class="text-xs font-weight-bold mb-0">3</p>
                                                                </td>
                                                                <!-- <td>
                                                                    <div>
                                                                        <img src="../assets/img/SIP-BANG.png" class="avatar avatar-sm me-3">
                                                                    </div>
                                                                </td> -->
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">Selasa, 02/11/2021</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">15.50.00 WIB</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <p class="text-xs font-weight-bold mb-0">AB 9090 LI</p>
                                                                </td>
                                                                <td class="text-center">
                                                                    <!-- tombol file -->
                                                                    <a href="#" class="mx-3" data-bs-toggle="modal" data-bs-target="#exampleModal3" data-toggle="tooltip" title="Lihat Detail" class="red-tooltip">
                                                                        <i class="fas fa-file text-info"></i>
                                                                    </a>

                                                                    <!-- Modal file-->
                                                                    <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                        <div class="modal-dialog">
                                                                            <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="row col-12">
                                                                                    <div class="col-md-6 text-left">
                                                                                        <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi</h5>
                                                                                        <!-- <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button> -->
                                                                                    </div>
                                                                                    <div class="col-md-6 text-right">
                                                                                        <small class="text-danger">Status : Belum Di Cek <i class="fas fa-exclamation-circle text-danger"></i></small>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <form role="form text-left">
                                                                                    <!-- Tanggal dan Waktu -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tanggal dan Waktu </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Selasa, 02/11/2021 15.50.00 WIB</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- nama pt -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Nama PT </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">PT. Ruby Permata Tunggal</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Plat Kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Plat Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">AB 9090 LI</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Harga/Ton -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Harga/Ton</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-dark">Rp 950.000</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- kir kendaraan-->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Kir Kendaraan </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-3 px-0">
                                                                                            <div class="form-group">
                                                                                                <input class="form-control form-control-sm" type="text" placeholder="3.000 kg">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4 px-0">
                                                                                            <div class="form-group">
                                                                                            <a href="" class="form-control-label text-center text-sm text-info"> Lihat Gambar Kir. </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Jenisgalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Jenis Galian </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- tonasegalian -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Tonase Galian</p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar </p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- pajak -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Pajak </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- total Harga -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm"> Total Harga </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left text-sm text-primary">Belum Keluar</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- Status -->
                                                                                    <div class="row col-12">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-left"> Status </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-1 px-0">
                                                                                            <div class="form-group">
                                                                                                <p class="form-control-label text-center text-sm"> : </p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-7 px-0">
                                                                                            <div class="form-group text-left">
                                                                                                <p class="badge badge-sm bg-success text-capitalize each word ">In</p>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Tutup</button>
                                                                                <button type="button" class="btn btn-success">Simpan</button>
                                                                            </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <!-- tombol hapus -->
                                                                    <a href="#" data-bs-toggle="tooltip"
                                                                        data-bs-original-title="Hapus">
                                                                        <i class="cursor-pointer fas fa-trash text-danger"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</main>
